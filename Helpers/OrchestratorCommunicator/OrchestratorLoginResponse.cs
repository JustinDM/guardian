namespace Economical.Guardian.Helpers.OrchestratorCommunicator
{
    public class OrchestratorLoginResponse
    {
        public string Result { get; set; }
    }
}