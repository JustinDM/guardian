namespace Economical.Guardian.Helpers.OrchestratorCommunicator
{
    public class OrchestratorAddQueueResponse
    {
        public string StatusCode { get; set; }
        public int Id { get; set; }
    }
}