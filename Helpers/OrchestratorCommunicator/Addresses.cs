namespace Economical.Guardian.Helpers.OrchestratorCommunicator
{
    public class Addresses
    {
        public string BaseAddress { get; set; }
        public string AddQueueItemAddress { get; set; }
        public string AuthenticateAddress { get; set; }
    }
}