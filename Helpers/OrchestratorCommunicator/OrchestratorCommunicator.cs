using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Economical.Guardian.Helpers.OrchestratorCommunicator
{
    public class OrchestratorCommunicator : HttpClient, IDisposable
    {
        private readonly IConfiguration _config;
        public OrchestratorCommunicator(IConfiguration config)
        {
            _config = config;
            LoadAddresses();
            BaseAddress = new Uri(_baseAddress);
        }
        // these should all be injected from the config stuff?
        private string _baseAddress;
        private string _addQueueItemAddress;
        private string _authenticateAddress;

        private void LoadAddresses()
        {
            // var addresses = _config.GetValue<Addresses>("Addresses");

            _baseAddress = _config["Addresses:BaseAddress"];
            _addQueueItemAddress = _config["Addresses:AddQueueItemAddress"];
            _authenticateAddress = _config["Addresses:AuthenticateAddress"];
        }

        private async Task<HttpResponseMessage> Login()
        {
            var formContent = new Dictionary<string, string>();

            // var configPasswordSection = _config.GetValue<LoginCredentials>("Login");


            // yeah it's dumb, deal with it
            var configPasswordSection = new LoginCredentials()
            {
                Password = _config["Login:Password"],
                Username = _config["Login:Username"],
                Tenancy = _config["Login:Tenancy"]
            };

            if (configPasswordSection == null)
            {
                throw new Exception($"Unable to retrieve login info");
            }

            formContent.Add("password", configPasswordSection.Password);
            formContent.Add("usernameOrEmailAddress", configPasswordSection.Username);
            formContent.Add("tenancyName", configPasswordSection.Tenancy);

            // trying to remove having the credentials in memory
            configPasswordSection = new LoginCredentials();

            var formContentString = JsonSerializer.Serialize(formContent);

            formContent = new Dictionary<string, string>();

            var stringContent = new StringContent(formContentString, Encoding.UTF8, "application/json");

            formContentString = string.Empty;

            var responseMessage = await PostAsync(_authenticateAddress, stringContent);

            if (responseMessage.IsSuccessStatusCode)
            {
                var responseContent = await responseMessage.Content.ReadAsStringAsync();

                var loginReponse = JsonSerializer.Deserialize<OrchestratorLoginResponse>(responseContent, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });

                DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", loginReponse.Result);

                return responseMessage;
            }

            else
            {
                throw new Exception("failed to log in");
            }
        }

        public async Task<HttpResponseMessage> AddQueueItem(QueueItem queueItem)
        {
            await Login();

            //something's up with our generation of the content

            var stringContent = new StringContent(JsonSerializer.Serialize(queueItem), Encoding.UTF8, "application/json");

            var responseMessage = await PostAsync(_addQueueItemAddress, stringContent);

            return responseMessage;
        }

        // queue name doesn't seem to matter
        public async Task<HttpResponseMessage> GetQueueItem(int id)
        {
            var responseMessage = await GetAsync(GetQueueItemString(id));

            return responseMessage;
        }

        private string GetQueueItemString(int id)
        {
            return $@"/odata/QueueItems({id})";
        }

        ~OrchestratorCommunicator()
        {
            Dispose();
        }
    }
}