namespace Economical.Guardian.Helpers.OrchestratorCommunicator
{
    public class LoginCredentials
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Tenancy { get; set; }
    }
}