using Economical.EcoObjects.General.Enums;

namespace Economical.Guardian.Helpers.OrchestratorCommunicator
{
    public class OrchestratorContent
    {
        public int TransactionId { get; set; }
        // putting this in as a string so we can get the actual string value from the enum
        // should help with readability
        public string Process { get; set; }
    }
}