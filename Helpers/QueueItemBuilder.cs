using System;
using Economical.EcoObjects.AlbertaCompliance;
using Economical.EcoObjects.ContractorConnection;
using Economical.EcoObjects.General.Enums;
using Economical.EcoObjects.General.Models.Transaction;
using Economical.Guardian.Helpers.OrchestratorCommunicator;

namespace Economical.Guardian.Helpers
{
    public static class QueueItemBuilder
    {
        private static QueueItem BuildGenericQueueItem(OrchestratorContent content, 
            OrchestratorQueue queue, QueuePriority priority, DateTime dueDate)
        {
            return new QueueItem()
            {
                itemData = new ItemData()
                {
                    Name = queue.ToString(),
                    Priority = priority.ToString(),
                    SpecificContent = content,
                    DueDate = dueDate
                }
            };

        }

        public static QueueItem BuildContractorConnectionQueueItem(int transactionId,
            OrchestratorQueue queue, QueuePriority priority, DateTime dueDate)
        {
            var content = new OrchestratorContent()
            {
                TransactionId = transactionId,
                Process = RpaProcess.ContractorConnection.ToString()
            };

            return BuildGenericQueueItem(content, queue, priority, dueDate);
        }

        public static QueueItem BuildAlbertaComplianceQueueItem(int transactionId,
            OrchestratorQueue queue, QueuePriority priority, DateTime dueDate)
        {
            var content = new OrchestratorContent()
            {
                TransactionId = transactionId,
                Process = RpaProcess.AlbertaComplianceLetters.ToString()
            };

            return BuildGenericQueueItem(content, queue, priority, dueDate);
        }
    }

}