using System;

namespace Economical.Guardian.Helpers
{
    public class ItemData
    {
        // This is the queue name
        public string Name { get; set; }
        public string Priority { get; set; }
        // this is where we store the actual transaction information
        public object SpecificContent { get; set; }
        // public DateTime DeferDate { get; set; }
        public DateTime DueDate { get; set; }
    }
}