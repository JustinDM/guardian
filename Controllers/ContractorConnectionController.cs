using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using Economical.EcoObjects.ContractorConnection;
using Economical.EcoObjects.ContractorConnection.Helpers;
using Economical.EcoObjects.General.Enums;
using Economical.EcoObjects.General.Helpers;
using Economical.EcoObjects.General.Models.Transaction;
using Economical.Guardian.Helpers;
using Economical.Guardian.Helpers.OrchestratorCommunicator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Economical.Guardian.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ContractorConnectionController : ControllerBase
    {
        private readonly OrchestratorCommunicator _orchComm;
        private readonly AuthCommunicator _authComm;

        // not sure about having the authComm here -> shouldn't this all be handled by the repoComm?
        public ContractorConnectionController(OrchestratorCommunicator orchComm, AuthCommunicator authComm)
        {
            _authComm = authComm;
            _orchComm = orchComm;
        }

        [HttpPost]
        [Authorize(Policy = "RequireAnalystRole")]
        public async Task<IActionResult> UploadReportFile(IFormFile file)
        {
            var transactions = new List<TransactionLog>();

            try
            {
                transactions = CcTransactionBuilder.GenerateTransactionLogs(file);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);
                return BadRequest("Unable to parse excel file");
            }

            var results = new List<OrchestratorAddQueueResponse>();

            // will need to log this to server repo first, then get back our transaction id to pass to the queueItemBuilder

            foreach (var i in transactions)
            {
                var transactInfo = i.TransactionInfo as CcTransactionInfo;
                var queueItem = QueueItemBuilder.BuildContractorConnectionQueueItem(transactInfo.InvoiceNumber, OrchestratorQueue.TestQueue, QueuePriority.Normal, DateTime.Now.AddDays(1));
                // how do we handle individual additions failing? Fail whole batch or fail some? We need to take in whole file since unlikely to be able to use openxml in angular

                var response = await _orchComm.AddQueueItem(queueItem);

                var result = JsonSerializer.Deserialize<OrchestratorAddQueueResponse>(await response.Content.ReadAsStringAsync(), new JsonSerializerOptions { PropertyNameCaseInsensitive = true });

                result.StatusCode = response.StatusCode.ToString();

                results.Add(result);
            }

            return Ok(results);
        }

        // public async Task<IActionResult> GetQueueItem(int id)
        // {
        //     var queueItemResponse = await _communicator.GetQueueItem(id);

        //     // response message brings back mulitple things - need to parse out the actual object from
        //     // you're going to have to use newtonsoft.json here since system.text.json doesn't handle that kind of behaviour without modiftying the parent property to an object
        //     // which kind defeats the whole point doesn't it?
        //     // thanks Bill

        //     // why do we need to get a queue item? Ideally we should be going to the log server right?

        //     return Ok(queueItemResponse);
        // }
    }
}